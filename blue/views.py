from django.contrib import messages
from django.shortcuts import render, redirect
from django.db import connection, transaction, IntegrityError
from .forms import *

def home(request):
    return render(request, "landing.html")

def create_dpay(request):
    if request.method == "POST":
        form = create_dpay(request.POST)
        if form.is_valid():
            form.save()
            return redirect('/blue/RUD_dpay_staff')
        else:
            messages.error(request, "Error, form is invalid")

    with connection.cursor() as cursor:
        cursor.execute("SELECT email FROM SILAU.user")
        columns = [col[0] for col in cursor.description]
        emails = [dict(zip(columns, row)) for row in cursor.fetchall()]

    return render(request, "create_dpay.html", {'emails': emails})

def RUD_dpay_staff(request):
    with connection.cursor() as cursor:
        cursor.execute("SELECT * FROM dpay_topup_transaction")
        columns = [col[0] for col in cursor.description]
        data = [dict(zip(columns, row)) for row in cursor.fetchall()]
        # print(data)
    return render(request, "RUD_dpay_staff.html", {'data': data})

def RUD_dpay_customer(request):
    with connection.cursor() as cursor:
        cursor.execute("SELECT * FROM dpay_topup_transaction")
        columns = [col[0] for col in cursor.description]
        data = [dict(zip(columns, row)) for row in cursor.fetchall()]
        # print(data)
    return render(request, "RUD_dpay_customer.html", {'data': data})

def update_dpay(request):
    if request.method == "POST":
        form = update_dpay(request.POST)
        if form.is_valid():
            form.save()
            return redirect('/blue/RUD_dpay_staff')
        else:
            messages.error(request, "Error, data not valid")

    with connection.cursor() as cursor:
        cursor.execute("SELECT * FROM dpay_topup_transaction")
        columns = [col[0] for col in cursor.description]
        data = [dict(zip(columns, row)) for row in cursor.fetchall()]

    form = create_dpay(request)

    return render(request, "update_dpay.html", {'data': data[0], 'form': form})
