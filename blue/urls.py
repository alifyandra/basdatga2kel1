from django.contrib import admin
from django.urls import include, path
from . import views

urlpatterns = [
    path('blue/', views.home, name='blue'),
    path('blue/create_dpay', views.create_dpay, name='blue-create-dpay'),
    path('blue/RUD_dpay_staff', views.RUD_dpay_staff, name='blue-RUD-dpay-staff'),
    path('blue/RUD_dpay_customer', views.RUD_dpay_customer, name='blue-RUD-dpay-customer'),
    path('blue/update_dpay', views.update_dpay, name='blue-update-dpay')
    ]
