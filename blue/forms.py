from django import forms
from django.db import connection, transaction, IntegrityError

class update_dpay(forms.Form):
    nominal = forms.IntegerField(required=True)

    def __init__(self, *args, **kwargs):
        super().__init__(*args, **kwargs)
        with connection.cursor() as cursor:
            cursor.execute("SELECT nominal FROM dpay_topup_transaction")
            self.fields['nominal'].choices = cursor.fetchall()

    def check_email(self):
        data = self.cleaned_data.get("email")
        with connection.cursor() as cursor:
            cursor.execute("SELECT * FROM dpay_topup_transaction WHERE email =" + data + ")")
            if cursor.fetchone() is None:
                raise forms.ValidationError("Data doesn't exist")
        return data

    def save(self, old):
        code = self.cleaned_data
        with connection.cursor() as cursor:
            cursor.execute("UPDATE dpay_topup_transaction SET nominal=" + code.get("nominal") + " WHERE email =" + code.get(email) + ")")
    
class create_dpay(forms.Form):
    email = forms.EmailField()
    emails = []
    nominal = forms.IntegerField(required=True)

    def __init__(self, *args, **kwargs):
        super().__init__(*args, **kwargs)
        with connection.cursor() as cursor:
            cursor.execute("SELECT email FROM SILAU.user")
            self.fields['email'].choices = cursor.fetchall()

    def check_email(self):
        data = self.cleaned_data.get("email")
        with connection.cursor() as cursor:
            cursor.execute("SELECT * FROM dpay_topup_transaction WHERE email =" + data + ")")
            if cursor.fetchone() is not None:
                raise forms.ValidationError("Email does not exist.")
        return data

    def save(self):
        data = self.cleaned_data
        with connection.cursor() as cursor:
            cursor.execute("SELECT date FROM dpay_topup_transaction WHERE email = " + data.get('email') + ")")
            date = cursor.fetchone()[0]
            cursor.execute("INSERT INTO TOPUP_DPAY_TRANSACTION VALUES(" + data.get('email') + "," + date + "," + data.get('nominal') + ")")