import math

from django.shortcuts import render, redirect
from django.http import HttpResponseRedirect
from django.db import connection


# Create your views here.
def red(request):
    return render(request, 'red/index.html')

def customer(request, pk):
    clause = ''
    search = ''

    if request.method == 'POST':
        pk = 1
        search = request.POST['search']
        clause = f" WHERE email LIKE '%{search}%'"

    with connection.cursor() as cursor:
        cursor.execute("SELECT * FROM testimony" + clause)
        columns = [col[0] for col in cursor.description]
        data = [dict(zip(columns,row)) for row in cursor.fetchall()]
        print(len(data))

    page_length = math.ceil(len(data)/10)
    i = (int(pk)-1) * 10 + 1

    context = {
        'testimonies': data[i-1:i+9],
        'pages': range(1, page_length+1),
        'search': search,
    }

    return render(request, 'red/testimony/customer.html', context)

def staff(request, pk):
    clause = ''
    search = ''

    if request.method == 'POST':
        pk = 1
        search = request.POST['search']
        clause = f" WHERE email LIKE '%{search}%'"

    with connection.cursor() as cursor:
        cursor.execute("SELECT * FROM testimony" + clause)
        columns = [col[0] for col in cursor.description]
        data = [dict(zip(columns,row)) for row in cursor.fetchall()]
        print(len(data))

    page_length = math.ceil(len(data)/10)
    i = (int(pk)-1) * 10 + 1

    context = {
        'testimonies': data[i-1:i+9],
        'pages': range(1, page_length+1),
        'search': search,
    }

    return render(request, 'red/testimony/staff.html', context)

def other(request, pk):
    clause = ''
    search = ''

    if request.method == 'POST':
        pk = 1
        search = request.POST['search']
        clause = f" WHERE email LIKE '%{search}%'"

    with connection.cursor() as cursor:
        cursor.execute("SELECT * FROM testimony" + clause)
        columns = [col[0] for col in cursor.description]
        data = [dict(zip(columns,row)) for row in cursor.fetchall()]
        print(len(data))

    page_length = math.ceil(len(data)/10)
    i = (int(pk)-1) * 10 + 1

    context = {
        'testimonies': data[i-1:i+9],
        'pages': range(1, page_length+1),
        'search': search,
    }
    
    return render(request, 'red/testimony/other.html', context)

def create(request):
    with connection.cursor() as cursor:
        cursor.execute(
            '''
            SELECT lt.email, lt.date 
            FROM laundry_transaction lt, transaction_status ts, status s
            WHERE lt.email=ts.email
            AND lt.date=ts.transaction_date
            AND ts.status_code=s.code
            AND s.name='done'
            '''
            )
        columns = [col[0] for col in cursor.description]
        data = [dict(zip(columns,row)) for row in cursor.fetchall()]
        print(len(data))

    context = {
        'data': data,
    }

    return render(request, 'red/testimony/create.html', context)

def update(request):
    return render(request, 'red/testimony/update.html')

def staff_update(request):
    return render(request, 'red/testimony/staff_update.html')

def service_staff(request, pk):
    clause = ''
    search = ''

    if request.method == 'POST':
        pk = 1
        search = request.POST['search'].upper()
        clause = f" WHERE code LIKE '%{search}%' OR name LIKE '%{search}%'"

    with connection.cursor() as cursor:
        cursor.execute("SELECT * FROM service" + clause)
        columns = [col[0] for col in cursor.description]
        data = [dict(zip(columns,row)) for row in cursor.fetchall()]
        print(len(data))
    page_length = math.ceil(len(data)/10)
    i = (int(pk)-1) * 10 + 1
    context = {
        'services': data[i-1:i+9],
        'pages': range(1, page_length+1),
        'search': search,
    }
    return render(request, 'red/service/staff.html', context)

def service_other(request, pk):
    clause = ''
    search = ''

    if request.method == 'POST':
        pk = 1
        search = request.POST['search'].upper()
        clause = f" WHERE code LIKE '%{search}%' OR name LIKE '%{search}%'"
    
    with connection.cursor() as cursor:
        cursor.execute("SELECT * FROM service" + clause)
        columns = [col[0] for col in cursor.description]
        data = [dict(zip(columns,row)) for row in cursor.fetchall()]
        print(len(data))
    page_length = math.ceil(len(data)/10)
    i = (int(pk)-1) * 10 + 1
    context = {
        'services': data[i-1:i+9],
        'pages': range(1, page_length+1),
        'search': search,
    }
    return render(request, 'red/service/other.html', context)

def update_service(request, code):
    with connection.cursor() as cursor:
        cursor.execute(f"SELECT * FROM service WHERE code='{code}'")
        columns = [col[0] for col in cursor.description]
        data = [dict(zip(columns,row)) for row in cursor.fetchall()]
        print(len(data))

    context = {
        'data': data[0],
    }
    return render(request, 'red/service/update.html', context)