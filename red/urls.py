from django.urls import path

from . import views

urlpatterns = [
    path('', views.red, name='red_index'),

    path('testimony/customer/<int:pk>/', views.customer, name='red_customer'),
    path('testimony/staff/<int:pk>/', views.staff, name='red_staff'),
    path('testimony/other/<int:pk>/', views.other, name='red_other'),
    path('testimony/customer/create/', views.create, name='red_create'),
    path('testimony/customer/update/', views.update, name='red_update'),
    path('testimony/staff/update/', views.staff_update, name='red_staff_update'),

    path('service/staff/<int:pk>/', views.service_staff, name='staff_red'),
    path('service/other/<int:pk>/', views.service_other, name='other_red'),
    path('service/staff/update/<str:code>/', views.update_service, name='update_red')
]