from django.contrib import admin
from django.urls import include, path
from . import views

urlpatterns = [
    path('green/', views.index, name="green"),
    path('green/customer/<str:pk>', views.customer, name="green-customer"),
    path('green/staff/<str:pk>', views.staff, name="green-staff"),
    path('green/staff-update/<str:pk>', views.staffupdate, name="green-staff-update"),
    path('green/staff-create/', views.staffcreate, name="green-staff-create"),
    path('green/staff-delete/<str:pk>', views.staffdelete, name="green-staff-delete"),
    path('green/item-customer/<str:pk>', views.itemcustomer, name="green-item-customer"),
    path('green/item-staff/<str:pk>', views.itemstaff, name="green-item-staff"),
    path('green/item-staff-create', views.itemcreate, name="green-item-create")
]
