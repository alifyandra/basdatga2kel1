from django.shortcuts import render, redirect
from django.db import connection, transaction, IntegrityError
from app.models import *
from .forms import *
from app.decorators import *
import math
from django.contrib import messages 

# Create your views here.
def index(request):
    return render(request, 'green/index.html',{'title':'green'})

@authenticated_user_required
def customer(request, pk):
    if request.session['user'] != 'courier':
        clause = ""
        if request.method == "POST" and request.POST['search'] != "":
            pk = 1
            search = request.POST['search']
            try:
                int(search)
                clause = f"""WHERE email ILIKE '%{search}%' OR serial_no = {search}"""
            except:
                clause = f"""WHERE email ILIKE '%{search}%' OR item_code ILIKE '%{search}%'"""
        
        with connection.cursor() as cursor:
            cursor.execute("SELECT * FROM laundry_list "+clause)
            columns = [col[0] for col in cursor.description]
            data = [dict(zip(columns,row)) for row in cursor.fetchall()]
        
        if request.method == "POST" and request.POST['search'] != "":
            context = {
                'title' : 'Customer Laundry List',
                'pageno' : pk,
                'pages' : range(1,2),
                'data' : data,
            }
        else:
            i = (int(pk)-1) * 10 + 1
            tablesize = math.ceil(len(data)/10)
            context = {
                'title' : 'Customer Laundry List',
                'pageno' : pk,
                'rows' : range(i, i + 10),
                'pages' : range(1,tablesize+1),
                'data' : data[i-1:i+9],
            }
        return render(request, 'green/customer.html', context)
    else:
        return redirect('/green')

@authenticated_user_required
def staff(request, pk):
    if request.session['user'] == 'staff':
        clause = ""
        if request.method == "POST" and request.POST['search'] != "":
            pk = 1
            search = request.POST['search']
            try:
                int(search)
                clause = f"""WHERE email ILIKE '%{search}%' OR serial_no = {search}"""
            except:
                clause = f"""WHERE email ILIKE '%{search}%' OR item_code ILIKE '%{search}%'"""
        
        with connection.cursor() as cursor:
            cursor.execute("SELECT * FROM laundry_list "+clause)
            columns = [col[0] for col in cursor.description]
            data = [dict(zip(columns,row)) for row in cursor.fetchall()]

        if request.method == "POST" and request.POST['search'] != "":
            context = {
                'title' : 'Staff Laundry List',
                'pageno' : pk,
                'pages' : range(1,2),
                'data' : data,
            }
        else:
            i = (int(pk)-1) * 10 + 1
            tablesize = math.ceil(len(data)/10)
            context = {
                'title' : 'Staff Laundry List',
                'pageno' : pk,
                'rows' : range(i, i + 10),
                'pages' : range(1,tablesize+1),
                'data' : data[i-1:i+9],
            }

        return render(request, 'green/staff.html', context)
    else:
        return redirect('/green')

@authenticated_user_required
def staffupdate(request, pk):
    if request.session['user'] == 'staff':
        if request.method == "POST":
            print(request.POST)
            form = UpdateList(request.POST)
            print("eug disokin")
            if form.is_valid():
                print("berhasil gasi")
                form.save(pk)
                return redirect('/green/staff/1')
            else:
                messages.error(request, "Error, data not valid or serial_no already exists")

        with connection.cursor() as cursor:
            cursor.execute("SELECT * FROM laundry_list WHERE serial_no = " + pk)
            columns = [col[0] for col in cursor.description]
            data = [dict(zip(columns,row)) for row in cursor.fetchall()]

        with connection.cursor() as cursor:
            cursor.execute("SELECT code FROM item")
            columns = [col[0] for col in cursor.description]
            item_codes = [dict(zip(columns,row)) for row in cursor.fetchall()]

        form = UpdateList()
            
        return render(request, 'green/staff-update.html', {'title':'UPDATE', 'data':data[0], 'item_codes':item_codes, 'form':form})
    else:
        return redirect('/green')

@authenticated_user_required
def staffcreate(request):
    if request.session['user'] == 'staff':
        if request.method == "POST":
            form = CreateList(request.POST)
            if form.is_valid():
                print("were valid")
                form.save()
                return redirect('/green/staff/1')
            else:
                messages.error(request,"Error")

        with connection.cursor() as cursor:
            cursor.execute("SELECT email FROM LAUNDRY_TRANSACTION")
            columns = [col[0] for col in cursor.description]
            emails = [dict(zip(columns,row)) for row in cursor.fetchall()]
            cursor.execute("SELECT code FROM ITEM")
            columns = [col[0] for col in cursor.description]
            codes = [dict(zip(columns,row)) for row in cursor.fetchall()]

        context = {
            'title' : 'CREATE LIST',
            'emails' : emails,
            'codes' : codes,
        }
        return render(request, 'green/staff-create.html', context)
    else:
        return redirect('/green')

@authenticated_user_required
def staffdelete(request, pk):
    if request.session['user'] == 'staff':
        try:
            with connection.cursor() as cursor:
                cursor.execute(f"DELETE FROM LAUNDRY_LIST WHERE serial_no={pk}")
        except:
            pass
        return redirect('/green/staff/1')
    else:
        return redirect('/green')

@authenticated_user_required
def itemcustomer(request, pk):
    clause = ""
    if request.method == "POST" and request.POST['search'] != "":
        pk = 1
        search = request.POST['search']
        clause = f"""WHERE code ILIKE '%{search}%' OR name ILIKE '%{search}%'"""
    with connection.cursor() as cursor:
        cursor.execute("SELECT * FROM ITEM " + clause)
        columns = [col[0] for col in cursor.description]
        data = [dict(zip(columns,row)) for row in cursor.fetchall()]
    
    if request.method == "POST" and request.POST['search'] != "":
         context = {
            'title' : 'Staff Laundry List',
            'pageno' : pk,
            'pages' : range(1,2),
            'data' : data,
        }
    else:
        i = (int(pk)-1) * 10 + 1
        tablesize = math.ceil(len(data)/10)
        context = {
            'title' : 'Staff Laundry List',
            'pageno' : pk,
            'rows' : range(i, i + 10),
            'pages' : range(1,tablesize+1),
            'data' : data[i-1:i+9],
        }
    return render(request, 'green/item-customer.html', context)

@authenticated_user_required
def itemstaff(request, pk):
    if request.session['user'] == 'staff':
        clause = ""
        if request.method == "POST" and request.POST['search'] != "":
            pk = 1
            search = request.POST['search']
            clause = f"""WHERE code ILIKE '%{search}%' OR name ILIKE '%{search}%'"""
        with connection.cursor() as cursor:
            cursor.execute("SELECT * FROM ITEM " + clause)
            columns = [col[0] for col in cursor.description]
            data = [dict(zip(columns,row)) for row in cursor.fetchall()]
        
        if request.method == "POST" and request.POST['search'] != "":
            context = {
                'title' : 'Staff Laundry List',
                'pageno' : pk,
                'pages' : range(1,2),
                'data' : data,
            }
        else:
            i = (int(pk)-1) * 10 + 1
            tablesize = math.ceil(len(data)/10)
            context = {
                'title' : 'Staff Laundry List',
                'pageno' : pk,
                'rows' : range(i, i + 10),
                'pages' : range(1,tablesize+1),
                'data' : data[i-1:i+9],
            }
        return render(request, 'green/item-staff.html', context)
    else:
        return redirect('/green')

@authenticated_user_required
def itemcreate(request):
    if request.session['user'] == 'staff':
        if request.method == "POST":
            form = CreateItem(request.POST)
            if form.is_valid():
                print("were valid")
                form.save()
                return redirect('/green/item-staff/1')
            else:
                messages.error(request,"Error")
            
        context = {
            'title' : 'CREATE ITEM'
        }
        return render(request, 'green/item-create.html', context)
    else:
        return redirect('/green')