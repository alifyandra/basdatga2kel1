from django import forms
from django.db import connection, transaction, IntegrityError
class UpdateList(forms.Form):
    item_codes = []

    def __init__(self, *args, **kwargs):
        super().__init__(*args, **kwargs)
        with connection.cursor() as cursor:
            cursor.execute("SELECT code, code FROM ITEM")
            self.fields['item_code'].choices = cursor.fetchall()

    serial_no = forms.IntegerField(min_value=1, required=True)
    item_amount = forms.IntegerField(min_value=1, required=True)
    item_code = forms.ChoiceField(choices=item_codes, required=True)

    def clean_serial_no(self):
        data = self.cleaned_data.get('serial_no')
        print(f"data = {data}")
        with connection.cursor() as cursor:
            cursor.execute("SELECT * FROM LAUNDRY_LIST WHERE serial_no="+str(data))
            if cursor.fetchone() is not None:
                print("error bray")
                raise forms.ValidationError("Serial number already exists in database")
        return data

    def save(self, oldserial):
        cd = self.cleaned_data
        with connection.cursor() as cursor:
            print(oldserial)
            print(cd.get("serial_no"))
            cursor.execute(f"""
            UPDATE LAUNDRY_LIST SET serial_no = {cd.get("serial_no")}, item_amount = {cd.get("item_amount")}, item_code = '{cd.get("item_code")}'
            WHERE serial_no = {oldserial}
            """)

class CreateList(forms.Form):
    emails = []
    item_codes = []

    def __init__(self, *args, **kwargs):
        super().__init__(*args, **kwargs)
        with connection.cursor() as cursor:
            cursor.execute("SELECT code, code FROM ITEM")
            self.fields['item_code'].choices = cursor.fetchall()
            cursor.execute("SELECT email, email from SILAU.USER")
            self.fields['email'].choices = cursor.fetchall()

    email = forms.EmailField()
    item_amount = forms.IntegerField(min_value=1, required=True)
    item_code = forms.ChoiceField(choices=item_codes, required=True)

    def clean_email(self):
        data = self.cleaned_data.get("email")
        print(f"data: {data}")
        with connection.cursor() as cursor:
            cursor.execute(f"SELECT * FROM LAUNDRY_TRANSACTION WHERE email='{data}'")
            if cursor.fetchone() is None:
                print("error bray")
                raise forms.ValidationError("Email doesn't exist in laundry_transaction table")
        return data

    def save(self):
        cd = self.cleaned_data
        new_email = cd.get('email')
        print("saving")
        with connection.cursor() as cursor:
            cursor.execute(f"SELECT date from LAUNDRY_TRANSACTION WHERE email = '{new_email}'")
            new_date = cursor.fetchone()[0]
            print(new_date)
            cursor.execute(f"SELECT MAX(serial_no) FROM LAUNDRY_LIST")
            new_serial = cursor.fetchone()[0] + 1
            print(new_serial)
            cursor.execute(f"""INSERT INTO LAUNDRY_LIST VALUES(
                                '{new_email}', '{new_date}', {new_serial}, {cd.get("item_amount")},
                                '{cd.get("item_code")}')""")

class CreateItem(forms.Form):
    code = forms.CharField(min_length=1, max_length=10, required=True)
    name = forms.CharField(min_length=1, max_length=50, required=True)

    def clean_code(self):
        data = self.cleaned_data.get("code")
        with connection.cursor() as cursor:
            cursor.execute(f"SELECT * FROM ITEM WHERE code = '{data.upper()}'")
            if cursor.fetchone() is not None:
                print("error bray")
                raise forms.ValidationError("Item code already exists")
        return data

    def save(self):
        cd = self.cleaned_data
        with connection.cursor() as cursor:
            cursor.execute(f"""INSERT INTO ITEM VALUES(
                '{cd.get('code').upper()}', '{cd.get('name')}'
            )""")