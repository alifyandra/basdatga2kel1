from django.shortcuts import render, redirect
from django.contrib.auth import authenticate, login
from django.http import HttpResponse
from collections import namedtuple
from django.db import connection
from .forms import *
from passlib.hash import sha256_crypt
from django.forms import formset_factory
from .decorators import *
# from django.utils.translation import gettext_lazy as _

# from .forms import CustomUserCreationForm, EmployeeForm

# Create your views here.
def namedtuplefetchall(cursor):
    "Return all rows from a cursor as a namedtuple"
    desc = cursor.description
    nt_result = namedtuple('Result', [col[0] for col in desc])
    return [nt_result(*row) for row in cursor.fetchall()]

def index(request):
    return render(request, 'app/index.html', {'user': request.session})

@authenticated_user_redirect_home
def login(request):
    form = LoginForm(request.POST)
    if request.method == "POST" and form.is_valid():
        email = form.cleaned_data['email']
        raw_pass = form.cleaned_data['password']

        cursor = connection.cursor()

        # try:
        #     cursor.execute(f"SELECT password FROM SILAU.USER WHERE email='{email}'")
        #     dbpass_hash = cursor.fetchone()
        #     if (dbpass_hash is not None) and (sha256_crypt.verify(raw_pass, dbpass_hash[0])):
        #         cursor.execute(f"SELECT email FROM customer WHERE email='{email}'")
        #         if cursor.fetchone() is not None:
        #             request.session['user'] = 'customer'

        #         cursor.execute(f"SELECT email FROM courier WHERE email='{email}'")
        #         if cursor.fetchone() is not None:
        #             request.session['user'] = 'courier'
                
        #         cursor.execute(f"SELECT email FROM staff WHERE email='{email}'")
        #         if cursor.fetchone() is not None:
        #             request.session['user'] = 'staff'

        #         request.session['email'] = email
        #         return redirect('')

        try:
            cursor.execute(f"SELECT password FROM SILAU.USER WHERE email='{email}'")
            dbpass_hash = cursor.fetchone()
            print(dbpass_hash)
            if (dbpass_hash is not None) and (sha256_crypt.verify(raw_pass, dbpass_hash[0])):
                cursor.execute(f"SELECT email FROM customer WHERE email='{email}'")
                if cursor.fetchone() is not None:
                    request.session['user'] = 'customer'

                cursor.execute(f"SELECT email FROM courier WHERE email='{email}'")
                if cursor.fetchone() is not None:
                    request.session['user'] = 'courier'
                
                cursor.execute(f"SELECT email FROM staff WHERE email='{email}'")
                if cursor.fetchone() is not None:
                    request.session['user'] = 'staff'

                request.session['email'] = email
                return redirect('/')
            else:
                pass
        except:
            pass    

        finally:
            cursor.close()
    
    return render(request, 'app/login.html', {'form': LoginForm()})


def role(request):
    return render(request, 'app/role.html')

# def customer(request):
#     if request.method == 'POST':
#         user_form = CustomUserCreationForm(request.POST)
#         if user_form.is_valid():
#             user = user_form.save()
#             username = user_form.cleaned_data.get('username')
#             raw_password = user_form.cleaned_data.get('password1')
#             login(request, user, backend='django.contrib.auth.backends.ModelBackend')
#             return redirect('landing_page')
#     else:
#         user_form = CustomUserCreationForm()
#     context = {'user_form': user_form}
#     return render(request, 'app/customer.html', context)

@authenticated_user_redirect_home
def customer(request):
    if request.method == "POST":
        form = RegisterCustomerForm(request.POST)
        if form.is_valid():
            cd = form.cleaned_data
            print(cd['password'])
            hashed_pw = sha256_crypt.encrypt(cd['password'])
            print(hashed_pw)
            cursor = connection.cursor()
            try:
                cursor.execute(f"SELECT email FROM SILAU.USER WHERE email='{cd['email']}'")
                if cursor.fetchone() is None:
                    cursor.execute(f"""INSERT INTO SILAU.USER VALUES(
                                    '{cd['email']}','{hashed_pw}','{cd['full_name']}','{cd['address']}',
                                    {cd['mobile_no']},'{cd['gender']}')
                                    """)
                    cursor.execute(f"""INSERT INTO CUSTOMER VALUES(
                                    '{cd['email']}','{cd['virtual_account_no']}',{cd['dpay_balance']})
                                    """)
                    request.session['user'] = 'customer'
                    request.session['email'] = cd['email']
                    return redirect('/')
                else:
                    pass
            finally:
                cursor.close()
    form = RegisterCustomerForm()
    return render(request, 'app/customer.html',{'form':form})


@authenticated_user_redirect_home
def staff(request):
    if request.method == "POST":
        form = RegisterStaffForm(request.POST)
        if form.is_valid():
            cd = form.cleaned_data
            print(cd['password'])
            hashed_pw = sha256_crypt.encrypt(cd['password'])
            print(hashed_pw)
            cursor = connection.cursor()
            try:
                cursor.execute(f"SELECT email FROM SILAU.USER WHERE email='{cd['email']}'")
                if cursor.fetchone() is None:
                    cursor.execute(f"""INSERT INTO SILAU.USER VALUES(
                                    '{cd['email']}','{hashed_pw}','{cd['full_name']}','{cd['address']}',
                                    {cd['mobile_no']},'{cd['gender']}')
                                    """)
                    cursor.execute(f"""INSERT INTO STAFF VALUES(
                                    '{cd['email']}','{cd['npwp']}','{cd['account_no']}','{cd['bank_name']}','{cd['branch_office']}')
                                    """)
                    request.session['user'] = 'staff'
                    request.session['email'] = cd['email']
                    return redirect('/')
                else:
                    pass
            finally:
                cursor.close()
    form = RegisterStaffForm()
    return render(request, 'app/staff.html',{'form':form})


@authenticated_user_redirect_home
def courier(request):
    if request.method == "POST":
        form = RegisterCourierForm(request.POST)
        if form.is_valid():
            cd = form.cleaned_data
            print(cd['password'])
            hashed_pw = sha256_crypt.encrypt(cd['password'])
            print(hashed_pw)
            cursor = connection.cursor()
            try:
                cursor.execute(f"SELECT email FROM SILAU.USER WHERE email='{cd['email']}'")
                if cursor.fetchone() is None:
                    cursor.execute(f"""INSERT INTO SILAU.USER VALUES(
                                    '{cd['email']}','{hashed_pw}','{cd['full_name']}','{cd['address']}',
                                    {cd['mobile_no']},'{cd['gender']}')
                                    """)
                    cursor.execute(f"""INSERT INTO COURIER VALUES(
                                    '{cd['email']}','{cd['npwp']}','{cd['account_no']}','{cd['bank_name']}','{cd['branch_office']}',
                                    '{cd['sim_no']}','{cd['vehicle_no']}','{cd['vehicle_type']}')
                                    """)
                    request.session['user'] = 'courier'
                    request.session['email'] = cd['email']
                    return redirect('/')
                else:
                    pass
            finally:
                cursor.close()
    form = RegisterCourierForm()
    return render(request, 'app/courier.html',{'form':form})
# def staff(request):
#     if request.method == 'POST':
#         user_form = CustomUserCreationForm(request.POST)
#         employee_form = EmployeeForm(request.POST, instance=user)
#         if user_form.is_valid() and employee_form.is_valid():
#             user = user_form.save()
#             employee = employee_form.
#             username = user_form.cleaned_data.get('username')
#             raw_password = user_form.cleaned_data.get('password1')
#             login(request, user, backend='django.contrib.auth.backends.ModelBackend')
#             return redirect('landing_page')
#     else:
#         user_form = CustomUserCreationForm()
#     context = {'user_form': user_form}
#     return render(request, 'app/customer.html', context)

def logout(request):
    request.session.flush()
    return redirect('/')