SUPERUSER = 1
STAFF = 2
COURIER = 3
CUSTOMER = 4

USER_TYPE_CHOICES = (
    (SUPERUSER, 'superuser'),
    (STAFF, 'staff'),
    (COURIER, 'courier'),
    (CUSTOMER, 'customer')
  )