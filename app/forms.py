from django import forms

# from .models import CustomUser, EmployeeProfile


# class CustomUserCreationForm(UserCreationForm):
#     password2 = None

#     class Meta(UserCreationForm):
#         model = CustomUser
#         fields = ('email', 'full_name', 'mobile_no', 'gender', 'password1')

#     def clean_password1(self):
#         password1 = self.cleaned_data.get('password1')
#         try:
#             password_validation.validate_password(password1, self.instance)
#         except forms.ValidationError as error:

#             # Method inherited from BaseForm
#             self.add_error('password1', error)
#         return password1

# class CustomUserChangeForm(UserChangeForm):

#     class Meta:
#         model = CustomUser
#         fields = ('email', 'full_name', 'mobile_no', 'gender')


# class EmployeeForm(forms.ModelForm):
#     class Meta:
#         model = EmployeeProfile
#         fields = ('address', 'npwp', 'account_no', 'bank_name', 'branch_office')

# class UserForm(forms.ModelForm):
#     class Meta:
#         model = CustomUser
#         fields = ('email', 'password', 'full_name', 'mobile_no', 'gender')

class LoginForm(forms.Form):
    email = forms.EmailField(label=("Email"), required=True, max_length=50)
    password = forms.CharField(label=("Pasword"), widget=forms.PasswordInput, required=True, max_length=128)

class RegisterCustomerForm(forms.Form):
    gender_choices = [
        ('M','Male'),
        ('F','Female')
    ]
    email = forms.CharField(label=("Email"), required=True, max_length=50)
    password = forms.CharField(label=("Password"), widget = forms.PasswordInput, required=True, max_length=128)
    full_name = forms.CharField(label=("Full Name"), required=True, max_length=50)
    mobile_no = forms.CharField(label=("Mobile no"), required=True, max_length=20)
    gender = forms.ChoiceField(choices=gender_choices, required=True)
    address = forms.CharField(label=("Address"), required=True)
    virtual_account_no = forms.CharField(label=("Account No."), required=True)
    dpay_balance = forms.FloatField(label=("DPAY Balance"), required=True)

class RegisterStaffForm(forms.Form):
    gender_choices = [
        ('M','Male'),
        ('F','Female')
    ]
    email = forms.EmailField(label=("Email"), required=True, max_length=50)
    password = forms.CharField(label=("Password"), widget = forms.PasswordInput, required=True, max_length=128)
    full_name = forms.CharField(label=("Full Name"), required=True, max_length=50)
    mobile_no = forms.CharField(label=("Mobile no"), required=True, max_length=20)
    gender = forms.ChoiceField(choices=gender_choices, required=True)
    address = forms.CharField(label=("Address"), required=True)
    npwp = forms.CharField(label=("NPWP"), required=True, max_length=20)
    account_no = forms.CharField(label=("Account No"), required=True, max_length=20)
    bank_name = forms.CharField(label=("Bank"), required=True, max_length=50)
    branch_office = forms.CharField(label=("Branch Office"), required=True, max_length=50)

class RegisterCourierForm(forms.Form):
    gender_choices = [
        ('M','Male'),
        ('F','Female')
    ]
    email = forms.EmailField(label=("Email"), required=True, max_length=50)
    password = forms.CharField(label=("Password"), widget = forms.PasswordInput, required=True, max_length=128)
    full_name = forms.CharField(label=("Full Name"), required=True, max_length=50)
    mobile_no = forms.CharField(label=("Mobile no"), required=True, max_length=20)
    gender = forms.ChoiceField(choices=gender_choices, required=True)
    address = forms.CharField(label=("Address"), required=True)
    npwp = forms.CharField(label=("NPWP"), required=True, max_length=20)
    account_no = forms.CharField(label=("Account No"), required=True, max_length=20)
    bank_name = forms.CharField(label=("Bank"), required=True, max_length=50)
    branch_office = forms.CharField(label=("Branch Office"), required=True, max_length=50)
    sim_no = forms.CharField(label=("SIM No"), required=True, max_length=20)
    vehicle_no = forms.CharField(label=("Vehicle No"), required=True, max_length=20)
    vehicle_type = forms.CharField(label=("Vehicle Type"), required=True, max_length=50)