from django.shortcuts import redirect

def authenticated_user_required(view_func):
	def wrapper_func(request, *args, **kwargs):
		try:
			session = request.session['user']
			return view_func(request, *args, **kwargs)
		except Exception as e:
			return redirect('/login')
	return wrapper_func

def authenticated_user_redirect_home(view_func):
	def wrapper_func(request, *args, **kwargs):
		try:
			session = request.session['user']
			return redirect('')
		except Exception as e:
			return view_func(request, *args, **kwargs)
	return wrapper_func

def unauthorized_user_redirect_home(authorized_role):
	def decorator(view_func):
		def wrapper_func(request, *args, **kwargs):
			user = request.session['user']
			if user not in authorized_role:
				return redirect('')
			else:
				return view_func(request, *args, **kwargs)
		return wrapper_func
	return decorator