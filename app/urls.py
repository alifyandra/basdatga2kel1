from django.contrib import admin
from django.urls import include, path

from . import views

urlpatterns = [
    path('', views.index, name='landing_page'),
    path('signup/', views.role, name='choose_role'),
    path('signup/customer', views.customer, name='customer_signup'),
    path('signup/staff', views.staff, name='staff_signup'),
    path('signup/courier', views.courier, name='courier_signup'),
    path('login/', views.login, name='login'),
    path('logout/', views.logout, name='logout')
]
