from django.db import models
from django.contrib.auth.models import AbstractUser
from django.utils.translation import gettext_lazy as _

from . import constants as user_constants
# from .managers import CustomUserManager


# Create your models here.
# class CustomUser(AbstractUser):

#     class Gender(models.TextChoices):
#         MALE = 'M', _('Male')
#         FEMALE = 'F', _('Female')

#     username = None
#     email = models.EmailField(unique=True, null=True, db_index=True)
#     full_name = models.CharField(max_length=50)
#     mobile_no = models.CharField(max_length=20)
#     gender = models.CharField(max_length=2, choices=Gender.choices)
#     is_courier = models.BooleanField(default=False)
#     user_type = models.PositiveSmallIntegerField(choices=user_constants.USER_TYPE_CHOICES, default=1)

#     REQUIRED_FIELDS = ['full_name', 'mobile_no', 'gender']
#     USERNAME_FIELD = 'email'

#     objects = CustomUserManager()


# class EmployeeProfile(models.Model):
#     user = models.OneToOneField(CustomUser, on_delete=models.CASCADE, primary_key=True, related_name='user_profile')
#     address = models.TextField(blank=True)
#     npwp = models.CharField(max_length=20, blank=True)
#     account_no = models.CharField(max_length=20, blank=True)
#     bank_name = models.CharField(max_length=50, blank=True)
#     branch_office = models.CharField(max_length=50, blank=True)

#     def __str__(self):
#         return self.user.email

