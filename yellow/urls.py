from django.contrib import admin
from django.urls import include, path
from . import views

urlpatterns = [
    path('yellow/',views.home, name="home"),
    path('yellow/customerlist', views.customerlist, name = "customerlist"),
    path('yellow/stafflist', views.stafflist, name = "stafflist"),
    path('yellow/updatestaff', views.updatestaff, name = "updatestaff"),
    path('yellow/createstaffpickup', views.createstaffpickup, name = "createstaffpickup"),
]