from django.shortcuts import render
from django.http import HttpResponse
from django.db import connection, transaction, IntegrityError

# Create your views here.

def home(request):
    return render(request, 'home.html')

def customerlist(request):
    with connection.cursor() as cursor:
        cursor.execute("SELECT * FROM pickup_delivery_rate")
        columns = [col[0] for col in cursor.description]
        data = [dict(zip(columns, row)) for row in cursor.fetchall()]
        # print(data)
    return render(request, "customerlist.html", {'data': data})

def stafflist(request):
    with connection.cursor() as cursor:
        cursor.execute("SELECT * FROM pickup_delivery_rate")
        columns = [col[0] for col in cursor.description]
        data = [dict(zip(columns, row)) for row in cursor.fetchall()]
        # print(data)
    return render(request, "stafflist.html", {'data': data})

def updatestaff(request):
    return render(request, 'updatestaff.html')

def createstaffpickup(request):
    return render(request, 'createstaffpickup.html')